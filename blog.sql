-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2015 at 09:14 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `content`, `owner_id`, `post_id`, `created_at`) VALUES
(15, 'hi hi', 1, 6, 'Jul 11, 2015 4:59:07 PM');

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE IF NOT EXISTS `friend` (
  `id` int(11) NOT NULL,
  `send_request_user_id` int(11) NOT NULL,
  `received_request_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `is_accepted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_friend`
--

CREATE TABLE IF NOT EXISTS `group_friend` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_user`
--

CREATE TABLE IF NOT EXISTS `group_user` (
  `id` int(11) NOT NULL,
  `message_group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL,
  `send_user_id` int(11) NOT NULL,
  `received_group_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `sent_at` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `message_group`
--

CREATE TABLE IF NOT EXISTS `message_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `date_time` varchar(200) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `from_user_id`, `to_user_id`, `date_time`, `type`) VALUES
(1, 1, 2, 'Feb 10, 2017 4:43:09 PM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `privacy_id` int(11) NOT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  `updated_at` varchar(200) DEFAULT NULL,
  `commentable` tinyint(1) NOT NULL DEFAULT '1',
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `image`, `owner_id`, `privacy_id`, `created_at`, `updated_at`, `commentable`, `time`) VALUES
(6, '10 năm yêu nhau không bằng một đêm "ăn nằm" đúng lúc', '<p>Ng&agrave;y h&ocirc;m nay, t&ocirc;i kho&aacute;c l&ecirc;n m&igrave;nh bộ v&aacute;y xanh dương dịu d&agrave;ng, trang điểm thật đẹp, bắt một chuyến taxi để đến dự đ&aacute;m cưới m&agrave; t&ocirc;i đ&atilde; từng mơ về n&oacute; rất nhiều, chỉ c&oacute; điều trong mơ t&ocirc;i l&agrave; c&ocirc; d&acirc;u c&ograve;n ngo&agrave;i đời, t&ocirc;i chỉ l&agrave; một vị kh&aacute;ch.</p>\r\n\r\n<p>T&ocirc;i bắt đầu y&ecirc;u anh từ c&aacute;ch đ&acirc;y tr&ograve;n 10 năm. Mối t&igrave;nh đầu thời học tr&ograve; chẳng mấy ai nghĩ sẽ đậm s&acirc;u đến thế nhưng t&ocirc;i v&agrave; anh đ&atilde; thực sự y&ecirc;u thương nhau hết m&igrave;nh, c&ugrave;ng nhau trưởng th&agrave;nh v&agrave; bước qua những gian kh&oacute; đầu đời. Anh l&agrave; người đ&agrave;n &ocirc;ng &iacute;t n&oacute;i, d&aacute;ng vẻ lạnh l&ugrave;ng nhưng lại nồng nhiệt v&agrave; ch&acirc;n th&agrave;nh hơn bất cứ ai t&ocirc;i từng gặp. Ở b&ecirc;n anh t&ocirc;i lu&ocirc;n cảm thấy an to&agrave;n v&agrave; y&ecirc;n b&igrave;nh.</p>\r\n\r\n<p>Ch&uacute;ng t&ocirc;i đ&atilde; nghĩ đến đ&aacute;m cưới, ai quen biết ch&uacute;ng t&ocirc;i cũng đ&atilde; sẵn s&agrave;ng nhận thiệp mừng. Ấy vậy m&agrave; chuyện chẳng ai ngờ&hellip;</p>\r\n\r\n<p><img alt="10 năm yêu nhau không bằng 1 đêm “ăn nằm” đúng lúc" class="aligncenter size-full wp-image-203889" src="http://tieuthuyethay.com/wp-content/uploads/2015/10/10-nam-yeu-nhau-khong-bang-mot-dem-an-nam-dung-luc-1.jpg" style="border:0px; clear:both; display:block; font-family:inherit; font-size:inherit; font-stretch:inherit; font-style:inherit; font-variant:inherit; font-weight:inherit; height:auto; line-height:inherit; margin:0px auto 1.5em; max-width:90%; padding:0px; vertical-align:baseline; width:600px" /></p>\r\n\r\n<p><em>C&oacute; điều trong mơ t&ocirc;i l&agrave; c&ocirc; d&acirc;u c&ograve;n ngo&agrave;i đời, t&ocirc;i chỉ l&agrave; một vị kh&aacute;ch. (ảnh minh họa)</em></p>\r\n\r\n<p>10 năm y&ecirc;u nhau, ch&uacute;ng t&ocirc;i chưa bao giờ đi qua giới hạn. T&ocirc;i kh&ocirc;ng muốn chuyện đ&oacute; xảy ra trước đ&aacute;m cưới. Nhiều lần anh b&agrave;y tỏ muốn &ldquo;vượt r&agrave;o&rdquo; nhưng t&ocirc;i khăng khăng cự tuyệt. T&ocirc;i kh&oacute;c v&agrave; n&oacute;i rằng &ldquo;Nếu anh y&ecirc;u em th&igrave; anh phải giữ g&igrave;n cho em chứ!&rdquo;. D&ugrave; kh&ocirc;ng thể hiện ra, t&ocirc;i nghĩ anh đ&atilde; thất vọng rất nhiều. Đ&oacute; l&agrave; l&yacute; do khi c&oacute; người con g&aacute;i kh&aacute;c t&igrave;nh nguyện d&acirc;ng hiến cho anh, anh đ&atilde; ng&atilde; v&agrave;o l&ograve;ng c&ocirc; ta nhanh ch&oacute;ng. Họ vụng trộm với nhau bắt đầu từ một chuyến c&ocirc;ng t&aacute;c d&agrave;i ng&agrave;y ở nước ngo&agrave;i. Mối quan hệ cứ thế tiếp diễn trong thầm lặng v&igrave; c&ocirc; g&aacute;i đ&oacute; cũng đ&atilde; c&oacute; bạn trai.<br />\r\nTừ ng&agrave;y c&oacute; c&ocirc; ấy, anh kh&ocirc;ng c&ograve;n mặn m&agrave; với t&ocirc;i nữa. Mặc cho t&ocirc;i t&igrave;m mọi c&aacute;ch n&iacute;u k&eacute;o anh th&igrave; anh vẫn xa t&ocirc;i dần dần. T&ocirc;i thấy anh đau khổ khi c&ocirc; g&aacute;i đ&oacute; c&ograve;n d&ugrave;ng dằng chưa muốn chia tay người y&ecirc;u để to&agrave;n t&acirc;m to&agrave;n &yacute; đến với anh. T&ocirc;i thấy anh trong cơn say th&igrave; thầm gọi t&ecirc;n c&ocirc; ấy. T&ocirc;i thấy anh ngay cả khi đi b&ecirc;n t&ocirc;i mắt vẫn cứ lơ đễnh đến h&igrave;nh b&oacute;ng kh&aacute;c. T&ocirc;i hiểu rằng t&ocirc;i mất anh thật rồi.</p>\r\n\r\n<p>T&ocirc;i đ&atilde; kh&oacute;c bao nhi&ecirc;u đ&ecirc;m, t&ocirc;i chẳng c&ograve;n nhớ nữa. Chỉ biết l&agrave; đến một ng&agrave;y khi thấy mặt trời l&oacute; rạng qua khe cửa, cảm gi&aacute;c duy nhất tồn tại trong t&ocirc;i l&agrave; trống rỗng.</p>\r\n\r\n<p>T&ocirc;i nghĩ: &ldquo;Th&ocirc;i được, nếu anh đ&atilde; muốn c&ocirc; ấy đến thế, để t&ocirc;i t&aacute;c th&agrave;nh cho 2 người.&rdquo;. T&ocirc;i chủ động t&igrave;m đến anh bạn trai kia, chủ động t&aacute;n tỉnh, mời gọi v&agrave; &ldquo;c&aacute;i ng&agrave;n v&agrave;ng&rdquo; t&ocirc;i lu&ocirc;n giữ g&igrave;n đ&oacute;, t&ocirc;i trao cho người t&ocirc;i chẳng hề y&ecirc;u thương. C&aacute;i kết cho chuyện t&igrave;nh 10 năm của ch&uacute;ng t&ocirc;i l&agrave; g&igrave;? L&agrave; cả hai đều &ldquo;ăn nằm&rdquo; với người kh&aacute;c.</p>\r\n\r\n<p><img alt="10 năm yêu nhau không bằng 1 đêm “ăn nằm” đúng lúc" class="aligncenter size-full wp-image-203896" src="http://tieuthuyethay.com/wp-content/uploads/2015/10/10-nam-yeu-nhau-khong-bang-mot-dem-an-nam-dung-luc-2.jpg" style="border:0px; clear:both; display:block; font-family:inherit; font-size:inherit; font-stretch:inherit; font-style:inherit; font-variant:inherit; font-weight:inherit; height:auto; line-height:inherit; margin:0px auto 1.5em; max-width:90%; padding:0px; vertical-align:baseline; width:600px" /></p>\r\n\r\n<p><em>T&ocirc;i bắt đầu lao đến những chuyện t&igrave;nh một đ&ecirc;m (ảnh minh họa)</em></p>\r\n\r\n<p>Sau khi biết chuyện anh bạn trai l&ecirc;n giường với t&ocirc;i, c&ocirc; g&aacute;i kia chia tay ngay lập tức. Anh hạnh ph&uacute;c v&igrave; c&oacute; được c&ocirc; ấy m&agrave; chẳng biết tới c&acirc;u chuyện ph&iacute;a sau. T&ocirc;i th&igrave; bắt đầu lao đến những chuyện t&igrave;nh một đ&ecirc;m. Nằm trong v&ograve;ng tay một kẻ xa lạ mới quen, t&ocirc;i chỉ biết rơi nước mắt.</p>\r\n\r\n<p>V&agrave; h&ocirc;m nay l&agrave; ng&agrave;y anh lấy c&ocirc; g&aacute;i ấy. T&ocirc;i kh&ocirc;ng biết c&oacute; n&ecirc;n ch&uacute;c ph&uacute;c cho 2 người họ hay kh&ocirc;ng? L&agrave; anh sai v&igrave; qu&aacute; yếu l&ograve;ng hay l&agrave; t&ocirc;i sai v&igrave; qu&aacute; khu&ocirc;n mẫu? 10 năm y&ecirc;u nhau kh&ocirc;ng bằng một lần l&ecirc;n giường đ&uacute;ng l&uacute;c.</p>\r\n', 'IMG_3515.jpg', 1, 4, 'Jul 11, 2015 4:10:06 PM', 'Jul 11, 2015 4:19:32 PM', 0, '24-Feb-2015');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`) VALUES
(18, 6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `post_viewable`
--

CREATE TABLE IF NOT EXISTS `post_viewable` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE IF NOT EXISTS `privacy` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privacy`
--

INSERT INTO `privacy` (`id`, `name`) VALUES
(1, 'Private'),
(2, 'Protected 1'),
(3, 'protected 2'),
(4, 'Public');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `owner_id` int(11) NOT NULL,
  `privacy_id` int(11) NOT NULL,
  `address` text,
  `created_ad` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
(1, 'Truyện dài'),
(2, 'Nhật ký chia tay'),
(3, 'name'),
(4, 'acnsljsn'),
(5, 'Nhật ký tình yêu');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `hobby` text,
  `address` text,
  `work` text,
  `phone_number` varchar(20) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `level_id` smallint(4) NOT NULL,
  `desc` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `first_name`, `last_name`, `hobby`, `address`, `work`, `phone_number`, `status`, `created_at`, `updated_at`, `level_id`, `desc`, `image`, `auth_key`, `password_reset_token`) VALUES
(1, 'dalv', 'dalv0911@gmail.com', '$2y$13$ZA0vSEc1AfyD9q.qGyGJ4OL1jdlWn5ijIU9TUo2WHo/6MJoI8xQES', 'Le', 'Da', 'Nope', 'Vô gia cư', 'Sinh viên', '+841688279742', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Tôi xin phép được giữ im lặng', '10393905_1586166241635188_5766472032032390248_n.jpg', 'OeAtuinLn5o-6kooe7RUAG-TwZiwLFHz', NULL),
(2, 'tuanna', 'tuanna@gmail.com', '$2y$13$ZR1zHI5qPs/BaT2cSGqjtenfB1niOfSICSjJUCIaoSNOgva2pdnAC', 'Anh', 'Tuấn', 'Chém gió', 'Vô gia cư', 'Sinh viên', '+84123456789', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu ca hát, thích màu hồng, ghét sự giả dối.', '4a62a0341870c7adf63ed757e208649f7fa7a2a8.jpg', 'xGtJ6Qug14aUz-ndJ1bw8hncjbX8u2aX', NULL),
(3, 'chungtv', 'chungtv@gmail.com', '$2y$13$GwFOb6r3I8BGk7VJ4Qm41el0id0dA3.k4T4ZqWDSMhtctS4yaXK2q', 'Trần', 'Chung', 'Ngắm trai', 'Vô gia cư', 'Dev', '+841688279742', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu màu tím', 'DSC_9405.jpg', 'ymQLeidwDyM7OkrFDGE9wPro9hYjhhy8', NULL),
(4, 'kient', 'kient@gmail.com', '$2y$13$G2NgMyNOr4pES08hkhXeoOk.9azsvutAWx3HOMsSRn6ISPW/Llf1m', 'Trịnh', 'Kiên', 'Tán gái', 'Vô gia cư', 'Tán gái kiếm tiền', '1234567891', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu màu hồng', '', '3WUta84-WNkHxVmLS0kGNqMfzXtMThJm', NULL),
(5, 'vunh', 'vunh@gmail.com', '$2y$13$uUM3J9rC/0DsrLRwBSRtU.qbIj0WAaS6uojCCsiPFnNN2abVf8ZwW', 'Hoàng', 'Vũ', 'Tán gái', 'Ngõ tự do', 'Tán gái kiếm tiền', '1234567890', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu màu tím, ghét sự giả dối', 'DSC_9405.jpg', 'EZwB2327-qcv6pE0XlJIwTHeyTowrLCv', NULL),
(6, 'thanhph', 'thanhph@gmail.com', '$2y$13$nY3w/M9YFDukrjPFBIN6sOVaxC4Tvty5SDdUkJtiHuZtX90Ce1xLS', 'Thanh', 'Phạm', 'Tán gái', 'Cầu thanh trì', 'Sinh viên', '123456782345', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Nope hehe', 'IMG_20151028_162239.jpg', 'j9PANZOfk2ZqTxSM59u1MkFL6353ZMhB', NULL),
(7, 'tungnt', 'tungnt@gmail.com', '$2y$13$33IHuIDgjhw2JKETmjeh7umEF7H1rdodZVXa8T0znUKcuku9/1fqK', 'Thanh', 'Tung', 'ancscslckn', 'anccjnjcnk', 'cnaclnslkcnclk', '198208049824', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'ncacnasklcnl', 'DSC_9377.jpg', 'vuAJaoIxmUzvZ97CN76K1u7fWB3mblQt', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_comment` (`post_id`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_friend`
--
ALTER TABLE `group_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_user_message` (`message_group_id`),
  ADD KEY `group_user_user` (`user_id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_message_group` (`received_group_id`),
  ADD KEY `message_user` (`send_user_id`);

--
-- Indexes for table `message_group`
--
ALTER TABLE `message_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_user` (`from_user_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_post` (`owner_id`),
  ADD KEY `privacy_post` (`privacy_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_post_tag` (`tag_id`),
  ADD KEY `post_post_tag` (`post_id`);

--
-- Indexes for table `post_viewable`
--
ALTER TABLE `post_viewable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_viewable_post` (`post_id`),
  ADD KEY `post_viewable_user` (`user_id`);

--
-- Indexes for table `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_schedule` (`owner_id`),
  ADD KEY `privacy_schedule` (`privacy_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group_friend`
--
ALTER TABLE `group_friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message_group`
--
ALTER TABLE `message_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `post_viewable`
--
ALTER TABLE `post_viewable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `post_comment` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Constraints for table `group_user`
--
ALTER TABLE `group_user`
  ADD CONSTRAINT `group_user_message` FOREIGN KEY (`message_group_id`) REFERENCES `message_group` (`id`),
  ADD CONSTRAINT `group_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_message_group` FOREIGN KEY (`received_group_id`) REFERENCES `message_group` (`id`),
  ADD CONSTRAINT `message_user` FOREIGN KEY (`send_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_user` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `privacy_post` FOREIGN KEY (`privacy_id`) REFERENCES `privacy` (`id`),
  ADD CONSTRAINT `user_post` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_post_tag` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `tag_post_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

--
-- Constraints for table `post_viewable`
--
ALTER TABLE `post_viewable`
  ADD CONSTRAINT `post_viewable_post` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `post_viewable_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `privacy_schedule` FOREIGN KEY (`privacy_id`) REFERENCES `privacy` (`id`),
  ADD CONSTRAINT `user_schedule` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
