<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$user_id = Yii::$app->user->getId();
$user_static = \common\models\User::findIdentity($user_id);
$notification_query = \frontend\models\NotificationQuery::getInstance();
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Blog</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa  fa-user-plus"></i>
                        <span
                            class="label label-warning"><?= $num_notify = $notification_query->get_total_number_notification($user_id) ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Bạn có <?= $num_notify ?> thông báo</li>
                        <a href="<?= \yii\helpers\Url::to(['notify/index']) ?>">
                            <li class="btn btn-success col-md-12">View All</li>
                        </a>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        if (!empty($user_static['image'])) {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                                [
                                    'alt' => "Avatar",
                                    'class' => 'user-image',
                                ]
                            );

                        } else {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                [
                                    'alt' => "Avatar",
                                    'class' => 'user-image',
                                ]
                            );
                        }
                        ?>
                        <span class="hidden-xs">
                            <?php
                            $full_name_static = $user_static['first_name'] . ' ' . $user_static['last_name'];
                            echo empty($full_name_static) ? $user_static['username'] : $full_name_static;
                            ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php
                            if (!empty($user_static['image'])) {
                                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                                    [
                                        'alt' => "Avatar",
                                        'class' => 'img-circle',
                                    ]
                                );

                            } else {
                                echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                    [
                                        'alt' => "Avatar",
                                        'class' => 'img-circle',
                                    ]
                                );
                            }
                            ?>
                            <p>
                                <?php
                                $full_name_static = $user_static['first_name'] . ' ' . $user_static['last_name'];
                                echo empty($full_name_static) ? $user_static['username'] : $full_name_static;
                                ?>
                                <small><?= $user_static['level_id'] == 2 ? 'Administrator' : 'Member ' ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => Yii::$app->user->getId()]) ?>"
                                   class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa  fa-hand-peace-o"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
