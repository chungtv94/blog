<?php
$user_static = \common\models\User::findIdentity(Yii::$app->user->getId());
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php
                use yii\helpers\Html;
                use yii\widgets\ActiveForm;

                if (!empty($user_static['image'])) {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                        [
                            'alt' => "Avatar",
                            'class' => 'user-image',
                        ]
                    );

                } else {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                        [
                            'alt' => "Avatar",
                            'class' => 'user-image',
                        ]
                    );
                }
                ?>
            </div>
            <div class="pull-left info">
                <p> <?php
                    $full_name_static = $user_static['first_name'] . ' ' . $user_static['last_name'];
                    echo empty($full_name_static) ? $user_static['username'] : $full_name_static;
                    ?>
                </p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!--search form-->
        <form action="?r=search/product" method="post" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="keyword" class="form-control" placeholder="Tìm kiếm nhật kí"/>
                <span class="input-group-btn">
                    <button type='submit' id='search-btn' class="btn btn-flat"><i
                            class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php
        //TODO Tạo 1 menu ảo cho phép khách vào thì chỉ có 2 chức năng search và đăng nhập để có thêm nhiều chức năng
        ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu for you', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Quản lý tìm kiếm',
                        'icon' => 'fa fa-search-plus',
                        'url' => ['/search'],
                        'items' => [
                            ['label' => 'Tìm kiếm bạn bè', 'icon' => 'fa fa-user', 'url' => ['/search/user']],
                            ['label' => 'Tìm kiếm nhật kí', 'icon' => 'fa fa-ambulance', 'url' => ['/search/post']],
                            ['label' => 'Tìm kiếm nâng cao', 'icon' => 'fa fa-binoculars', 'url' => ['search/advanced']],
                        ]
                    ],
                    [
                        'label' => 'Quản lý nhật kí',
                        'icon' => 'fa fa-cubes',
                        'url' => ['/post'],
                        'items' => [
                            ['label' => 'Đăng bài mới', 'icon' => 'fa fa-pencil-square-o', 'url' => ['/post/create']],
                            ['label' => 'Danh sách bài đăng', 'icon' => 'fa fa-list-ul', 'url' => ['/post/index']],
                        ]
                    ],
                    ['label' => 'Quản lý lịch làm việc', 'icon' => 'fa fa-dashboard', 'url' => ['/schedule']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
