<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:43 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="user-form">
            <?php $form = ActiveForm::begin(
                [
                    'id' => 'product-from',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'phone_number')->textInput() ?>

            <?= $form->field($model, 'hobby')->textarea() ?>

            <?= $form->field($model, 'work')->textarea() ?>

            <?= $form->field($model, 'address')->textarea(['maxLength' => true]) ?>

            <?= $form->field($model, 'desc')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*', 'maxSize' => 10097152]) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'setting-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>