<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:31 PM
 */
use yii\helpers\Url;
use yii\helpers\Html;

$user = $model;
$this->title = empty($user['full_name']) ? $user['username'] : $user['full_name'];
$this->params['breadcrumbs'][0] = "Danh sách thành viên";
$this->params['breadcrumbs'][1] = $this->title;
?>
<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <?php
                if (empty($user['image'])) {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                        [
                            'alt' => "Avatar",
                            'class' => 'profile-user-img img-responsive img-circle',
                        ]
                    );
                } else {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user['image'],
                        [
                            'alt' => "Avatar",
                            'class' => 'profile-user-img img-responsive img-circle',
                        ]
                    );
                }
                ?>
                <h3 class="profile-username text-center">
                    <a href="<?= Url::to(['user/profile', 'id' => $user['id']]) ?>">
                        <?= empty($user['full_name']) ? $user['username'] : $user['full_name'] ?>
                    </a>
                </h3>

                <p class="text-muted text-center"><?= $user['level_id'] == 2 ? 'Administrator' : 'Member' ?></p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Số bài đăng</b> <a class="pull-right"><?= $user['prod_num'] ?></a>
                    </li>
                    <?php
                    //                    * 1.No relative
                    //                    * 2.Cho accept
                    //                    * 3.has request
                    //                    * 4.Friend

                    /** @var int $relative_code */
                    if (isset($relative_code)) {
                        switch ($relative_code) {
                            case 1:
                                echo '<a id ="' . $user['id'] . '"class="add_friend">' .
                                    '<li class="list-group-item">' .
                                    '    <i class="fa fa-user-plus"></i> Thêm bạn bè</li>' .
                                    '</a>';
                                break;
                            case 2:
                                echo ' <a class="list-group-item">' .
                                    '    <i class="fa fa-mail-forward"></i> Đã gửi yêu cầu' .
                                    ' </a>';
                                break;
                            case 3:
                                echo '<a href="?r=user/accept-friend&user_id=' . $user['id'] . '">' .
                                    '<li class="list-group-item">' .
                                    '    <i class="fa  fa-check-square-o"></i> Xác nhận bạn bè </li>' .
                                    '</a>';
                                break;
                            case 4:
                                echo '<a class="list-group-item">' .
                                    '    <i class="fa fa-user-plus"></i> Bạn bè' .
                                    '</a>';
                                break;
                            default:
                                break;
                        }
                    }
                    ?>
                    <a class="list-group-item" style="display: none" id="sent_request_friend">
                        <i class="fa fa-mail-forward"></i> Đã gửi yêu cầu
                    </a>
                    <a class="list-group-item" style="display: none" id="is_friend">
                        <i class="fa fa-mail-forward"></i> Friend
                    </a>

                    <div class="list-group-item" style="display: none" id="choice_group">
                        <form method="post" class="form-group">
                            <div class="radio">
                                <label>
                                    <input name="group_friend" id="optionsRadios1" value="1" type="radio">
                                    Người thân
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="group_friend" id="optionsRadios2" value="2" checked type="radio">
                                    Bạn bè
                                </label>
                            </div>
                            <button type="submit" class="btn btn-success" name="add_friend">Make Friend</button>
                        </form>
                    </div>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa fa-envelope"></i> Email</strong>

                <p class="text-muted">
                    <?= $user['email'] ?>
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> Địa chỉ</strong>

                <p class="text-muted"><?= $user['address'] ?></p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Sở thích</strong>

                <p><?= $user['hobby'] ?></p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Công việc</strong>

                <p><?= $user['work'] ?></p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Note</strong>

                <p><?= $user['desc'] ?></p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class=""><a href="#activity" data-toggle="tab" aria-expanded="false" aria-selected="true">Hoạt động
                        gần đây</a></li>
                <?php if ($user['id'] == Yii::$app->user->getId()) { ?>
                    <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="true">Settings</a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="activity">
                    <ul class="timeline">

                        <!-- timeline time label -->
                        <li class="time-label">
                             <span class="bg-red">
                                10 Feb. 2014
                            </span>
                        </li>
                        <!-- /.timeline-label -->

                        <!-- timeline item -->
                        <li>
                            <!-- timeline icon -->
                            <i class="fa fa-envelope bg-blue"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                                <h3 class="timeline-header"><a href="#">Support Team</a> ...</h3>

                                <div class="timeline-body">
                                    ...
                                    Content goes here
                                </div>

                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">...</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->

                        ...

                    </ul>
                </div>
                <div class="tab-pane active" id="settings">
                    <?php
                    if (isset($isUpdated)) {
                        if ($isUpdated) {
                            ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Chúc mừng!</h4>
                                Bạn đã chỉnh sửa thông tin thành công.
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Thật đáng tiếc!</h4>
                                Thông tin cá nhân của bạn hiện chưa được chỉnh sửa.
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    if ($user['id'] == Yii::$app->user->getId()) {
                        echo $this->render('_form', ['model' => $setting]);
                    }
                    ?>
                </div>
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>