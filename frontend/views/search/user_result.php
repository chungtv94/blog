<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/23/2015
 * Time: 1:10 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Tìm kiếm người dùng';
$this->params['breadcrumbs'][0] = "Tìm kiếm";
$this->params['breadcrumbs'][1] = $this->title;
?>
<div class="row">
    <div class="col-lg-8">
        <form action="?r=search/user" method="post" class="form-horizontal">
            <div class="input-group input-group-sm">
                <input placeholder="Nhập tên người dùng" type="text" name="keyword" class="form-control"
                       value="<?= empty($keyword) ? '' : $keyword ?>">
                <span class="input-group-btn">
                    <button class="btn btn-info btn-flat" type="submit">Go!</button>
                </span>
            </div>
        </form>
        <br><br>
        <?php
        if (isset($results)) {
            if (empty($results)) {
                echo '<div class="callout callout-danger">
                    <h4>Không có kết quả nào phù hợp - Not found</h4>
                    </div>';
            }
            /** @var $results */
            $index = 0;
            $count = count($results);
            foreach ($results as $user) {
                ?>
                <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $user['id']]) ?>">
                    <div class="box box-widget widget-user-2">

                        <div class="widget-user-header <?= ($index++ % 2 == 0) ? 'bg-yellow' : 'bg-aqua-active' ?>">
                            <div class="widget-user-image">
                                <?= Html::img(Yii::$app->request->baseUrl . '/images/' . (empty($user['image']) ? 'default.jpg' : $user['image']), [
                                    'class' => 'img-circle',
                                    'alt' => 'User Avatar',
                                ]) ?>
                            </div>

                            <h3 class="widget-user-username"><?= empty($user['full_name']) ? 'Not set full name yet' : $user['full_name'] ?></h3>
                            <h5 class="widget-user-desc"><i
                                    class="fa fa-map-marker margin-r-5"></i> <?= empty($user['address']) ? 'Not set address yet' : $user['address'] ?>
                            </h5>
                        </div>
                    </div>
                </a>
                <?php
            }

            if ($count >= 5) {
                echo '<div class="box-footer clearfix">';
                echo '<ul class="pagination pagination-sm no-margin pull-right">';

                if ($page > 0) {
                    echo '<li><a href="' . Url::to(['search/user-result', 'keyword' => $keyword, 'page' => ($page - 1)]) . '">Pre</a></li>';
                    echo '<li><a href="' . Url::to(['search/user-result', 'keyword' => $keyword, 'page' => $page]) . '">' . ($page) . '</a></li>';
                }
                if ($count == 5) {
                    echo '<li><a href="' . Url::to(['search/user-result', 'keyword' => $keyword, 'page' => ($page + 1)]) . '">Next</a></li>';
                }

                echo '</ul>';
                echo '</div>';
            }

        }
        ?>
    </div>
</div>