<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/23/2015
 * Time: 1:11 PM
 */
$this->title = 'Tìm kiếm người dùng';
$this->params['breadcrumbs'][0] = "Tìm kiếm";
$this->params['breadcrumbs'][1] = $this->title;
?>
<div class="col-lg-8" xmlns="http://www.w3.org/1999/html">
    <form action="?r=search/user" method="post" class="form-horizontal">
        <div class="input-group input-group-sm">
            <input placeholder="Nhập tên người dùng" type="text" name="keyword" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-info btn-flat" type="submit">Go!</button>
                </span>
        </div>
    </form>
    <br>
    <br>
</div>