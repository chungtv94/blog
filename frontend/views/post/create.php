<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 5:49 AM
 */
$this->title = 'Viết nhật kí';
$this->params['breadcrumbs'][0] = "Quản lý nhật kí";
$this->params['breadcrumbs'][1] = $this->title;
?>
<?php
if (isset($is_created) && $is_created == false) {
    ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Viết bài không thành công!</h4>
        Vì một số lý do trên hệ thống mà bài viết của bạn chưa được lưu.
    </div>
    <?php
}
?>
<?= $this->render('_form', ['model' => $model, 'tags' => $tags]); ?>
