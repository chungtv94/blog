<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/21/2015
 * Time: 12:36 PM
 */
$this->title = 'Sửa nhật ký';
$this->params['breadcrumbs'][0] = "Quản lý nhật kí";
$this->params['breadcrumbs'][1] = $this->title;
?>
<?php
if (isset($is_updated) && $is_updated == false) {
    ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Chỉnh sửa bài viết không thành công!</h4>
        Vì một số lý do trên hệ thống mà bài viết của bạn chưa được sửa.
    </div>
    <?php
}
?>
<?= $this->render('_form', ['model' => $model, 'tags' => $tags]); ?>