<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 10:54 AM
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Dah sách nhật ký';
$this->params['breadcrumbs'][0] = "Quản lý nhật kí";
$this->params['breadcrumbs'][1] = $this->title;
/** @var Array[] $posts */
$count = count($posts);
//TODO show list view
?>
<?php
foreach ($posts as $post){
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <div class="user-block">
            <?php
            if (!empty($post['image'])) {
                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $post['image'],
                    [
                        'alt' => "Avatar",
                        'class' => 'attachment-img',
                    ]
                );

            } else {
                echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                    [
                        'alt' => "Avatar",
                        'class' => 'attachment-img',
                    ]
                );
            }
            ?>
            <span class="username">
                <a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $post['id']]) ?>">
                    <?= $post['title'] ?></a>
                <span class="bg-red">
                        <?= $post['time']?>
                </span>
            </span>
            <span
                class="description"><?= \frontend\utils\Helper::print_privacy($post['privacy_id']) . ' - ' . $post['created_at'] ?></span>
        </div>
        <!-- /.user-block -->

        <div class="box-tools">
            <a href="<?= \yii\helpers\Url::to(['post/edit', 'id' => $post['id']]) ?>"
               class="btn btn-box-tool"
               alt="Chỉnh sửa"><i class="fa fa-edit"></i></a>
            <a href="<?= \yii\helpers\Url::to(['post/delete', 'id' => $post['id']]) ?>"
               class="btn btn-box-tool"
               alt="Xóa"><i class="fa fa-trash"></i></a>
        </div>
        <div class="box-body">
            <?=str_split($post['content'],300)[0]?>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <?php
    }
    ?>
    <?php
    if ($count >= 10) {
        echo '<div class="box-footer clearfix">';
        echo '<ul class="pagination pagination-sm no-margin pull-right">';

        if ($page > 0) {
            echo '<li><a href="' . Url::to(['post/list', 'page' => ($page - 1)]) . '">Pre</a></li>';
            echo '<li><a href="' . Url::to(['post/list', 'page' => $page]) . '">' . ($page) . '</a></li>';
        }
        if ($count == 5) {
            echo '<li><a href="' . Url::to(['post/list', 'page' => ($page + 1)]) . '">Next</a></li>';
        }

        echo '</ul>';
        echo '</div>';
    }
    ?>
