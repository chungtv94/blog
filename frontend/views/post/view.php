<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 12:21 PM
 */
use yii\helpers\Html;

$post = $model;
$relative_posts = $models;
$this->title = $post['title'];
$this->params['breadcrumbs'][0] = "Danh sách nhật ký";

$user_id = Yii::$app->user->getId();
$user_static = \common\models\User::findIdentity($user_id);
?>
<div class="row">
    <div class="col-lg-8">
        <div class="box box-widget">
            <div class="box-header with-border">
                <div class="user-block">
                    <?php
                    if (!empty($post['avatar'])) {
                        echo Html::img(Yii::$app->request->baseUrl . '/images/' . $post['avatar'],
                            [
                                'alt' => "Avatar",
                                'class' => 'img-circle',
                            ]
                        );

                    } else {
                        echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                            [
                                'alt' => "Avatar",
                                'class' => 'img-circle',
                            ]
                        );
                    }
                    ?>
                    <span class="username"><a
                            href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $post['owner_id']]) ?>">
                            <?= empty($post['full_name']) ? 'No Name' : $post['full_name'] ?></a></span>
                    <span
                        class="description"><?= \frontend\utils\Helper::print_privacy($post['privacy_id']) . ' - ' . $post['created_at'] ?></span>
                </div>
                <!-- /.user-block -->
                <?php
                if ($post['owner_id'] == $user_id) {
                    ?>
                    <div class="box-tools">
                        <a href="<?= \yii\helpers\Url::to(['post/edit', 'id' => $post['id']]) ?>"
                           class="btn btn-box-tool"
                           alt="Chỉnh sửa"><i class="fa fa-edit"></i></a>
                        <a href="<?= \yii\helpers\Url::to(['post/delete', 'id' => $post['id']]) ?>"
                           class="btn btn-box-tool"
                           alt="Xóa"><i class="fa fa-trash"></i></a>
                    </div>
                    <?php
                }
                ?>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <li class="time-label">
                    <span class="bg-red">
                        <?= $post['time'] ?>
                    </span>
                </li>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <?php
                        echo Html::img(Yii::$app->request->baseUrl . '/images/' . $post['image'],
                            [
                                'alt' => "photo",
                                'class' => 'img-responsive',
                            ]
                        );
                        ?>
                    </div>
                </div>
                <br>

                <p>I<?= $post['content'] ?></p>
            </div>
            <!-- /.box-body -->
            <div class="box-footer box-comments" id="box-comment">
                <?php
                /** @var Array[] $comments */
                foreach ($comments as $comment) {
                    ?>
                    <!-- /.box-comment -->
                    <div class="box-comment">
                        <!-- User image -->
                        <?php
                        if (!empty($post['avatar'])) {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/' . $comment['avatar'],
                                [
                                    'alt' => "Avatar",
                                    'class' => 'img-circle img-sm',
                                ]
                            );

                        } else {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                [
                                    'alt' => "Avatar",
                                    'class' => 'img-circle img-sm',
                                ]
                            );
                        }
                        ?>
                        <div class="comment-text">
                      <span class="username">
                        <?= empty($comment['full_name']) ? 'No name' : $comment['full_name'] ?>
                          <?php
                          if ($comment['owner_id'] == $user_id) {
                              echo '<a class="del_comment text-muted pull-right" id="' . $comment['id'] . '"><i
                                      class="fa fa-trash"></i></a>
                                    <br>';
                          }
                          ?>
                          <span class="text-muted pull-right"><?= $comment['created_at'] ?></span>
                      </span><!-- /.username -->
                            <?= $comment['content'] ?>
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    <!-- /.box-comment -->
                    <?php
                }
                ?>
            </div>
            <!-- /.box-footer -->
            <div class="box-footer">
                <!--                <form action="" method="post" class="comment-form">-->
                <?php
                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                    [
                        'alt' => "photo",
                        'class' => 'img-responsive img-circle img-sm',
                    ]
                );
                ?>
                <div class="img-push">
                    <input type="text" id="owner_id=<?= $user_id ?>&post_id=<?= $post['id'] ?>"
                           class="content_cmt form-control input-sm"
                           placeholder="Press enter to post comment">
                </div>
                <!-- .img-push is used to add margin to elements next to floating images -->
                <!--                </form>-->
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
    <div class="col-lg-4">
        <?php
        foreach ($relative_posts as $relative_post) {
            ?>
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        <?php
                        if (!empty($relative_post['image'])) {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/' . $relative_post['image'],
                                [
                                    'alt' => "Image",
                                    'class' => 'img-circle',
                                ]
                            );

                        } else {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/default_post.jpg',
                                [
                                    'alt' => "Image",
                                    'class' => 'img-circle',
                                ]
                            );
                        }
                        ?>
                        <span class="username"> <a
                                href="<?= \yii\helpers\Url::to(['post/view', 'id' => $relative_post['id']]) ?>"><?= $relative_post['title'] ?></a></span>
                        <span
                            class="description"><?= \frontend\utils\Helper::print_privacy($relative_post['privacy_id']) ?></span>
                    </div>
                </div>
                <div class="box-body">
                    <span class="bg-blue">
                        <?= $relative_post['time'] ?>
                    </span>

                    <p><?= str_split($relative_post['content'], 200)[0] ?></p>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>