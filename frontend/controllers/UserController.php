<?php
/**
 * Created by PhpStorm.
 * User: chungtv
 * Date: 10/19/2015
 * Time: 8:05 PM
 */

namespace frontend\controllers;

use frontend\models\PostQuery;
use frontend\models\SettingForm;
use frontend\models\UserQuery;
use Yii;
use common\models\User;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->getId() !== null) {
            $model = $this->findModel(Yii::$app->user->getId());
            return $this->render('index', ['model' => $model]);
        } else {
            throw new HttpException(403, 'You are not permission for this page');
        }
    }


    public function actionProfile($id)
    {

        $userQuery = UserQuery::getInstance();
        // $productQuery = PostQuery::getInstance();
        //TODO get recent post
        $model = $userQuery->getUserById($id);
        $my_id = Yii::$app->user->getId();
        if ($id == Yii::$app->user->getId()) {
            $setting = new SettingForm();
            if ($setting->load(Yii::$app->request->post()) && $setting->validate()) {
                $isUpdated = false;
                if ($setting->updateInfo()) {
                    $isUpdated = true;
                }
                return $this->render('profile', ['model' => $model, 'setting' => $setting, 'isUpdated' => $isUpdated]);
            } else {
                return $this->render('profile', ['model' => $model, 'setting' => $setting]);
            }
        } else {
            if(isset($_POST['group_friend'])){
                $group_id = $_POST['group_friend'];
                $send_request_user_id = $my_id;
                $received_request_user_id = $id;
                $userQuery->make_friend($send_request_user_id,$received_request_user_id,$group_id);
            }
            $relative_code = $userQuery->get_relative($my_id, $id);
            if (!empty($model)) {
                return $this->render('profile', ['model' => $model, 'relative_code' => $relative_code]);
            } else {
                throw new HttpException(404, 'The requested user could not be found.');
            }
        }
    }


    private function findModel($id)
    {

        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("The request page do not exist!");
        }
    }

}