<?php
/**
 * Created by PhpStorm.
 * User: minhnh
 * Date: 10/20/2015
 * Time: 5:38 AM
 */
namespace frontend\controllers;

use app\models\Post;
use app\models\PostTag;
use app\models\Tag;
use frontend\models\CommentQuery;
use frontend\models\FriendQuery;
use frontend\models\PostForm;
use frontend\models\PostQuery;
use frontend\models\TagQuery;
use frontend\utils\Helper;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class PostController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionCreate()
    {
        if (!Helper::permission_request())
            throw new BadRequestHttpException();
        $model = new PostForm();
        $tag_query = TagQuery::getInstance();
        $tags = Tag::toMapString($tag_query->getAll());
        $model->owner_id = Yii::$app->user->getId();
        $model->created_at = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                $tag_query->manage_tags($model->id, $model->tags);
                if ($model->privacy_id == 2) {
                    //TODO select friend
                    return $this->runAction('post/select-friend', ['post_id' => $model->id]);
                } else {
                    return $this->runAction('post/index');
                }
            } else {
                return $this->render('create', ['model' => $model, 'tags' => $tags, 'is_created' => false]);
            }
        }
        return $this->render('create', ['model' => $model, 'tags' => $tags]);
    }

    public function actionView($id)
    {
        $post_query = PostQuery::getInstance();
        $comment_query = CommentQuery::getInstance();
        $user_id = Yii::$app->user->getId();
        $post = $post_query->getPostById($id);
        if (!$post_query->readable($user_id, $post['owner_id'], $post['id'], $post['privacy_id']))
            throw new BadRequestHttpException();
        $relative_posts = $post_query->getBeforePostForId($post['owner_id']);
        $comments = $comment_query->get_comments($id);
        if (isset($_POST['comment-content']) && $_POST['comment-content'] != '') {
            $content = $_POST['comment-content'];
            $comment_query->create($id, $user_id, $content);
            $comments = $comment_query->get_comments($id);
            return $this->render('view', ['model' => $post, 'models' => $relative_posts, 'comments' => $comments]);
        }
        return $this->render('view', ['model' => $post, 'models' => $relative_posts, 'comments' => $comments]);
    }

    public function actionEdit($id)
    {
        $post = Post::findOne(['id' => $id]);
        if (!Helper::permission_request() || Yii::$app->user->getId() !== $post['owner_id'])
            throw new BadRequestHttpException();
        $tag_query = TagQuery::getInstance();
        $tags = Tag::toMapString($tag_query->getAll());

        $model = new PostForm();
        $this->assign_post_to_form($post, $model);
        $selected_tags = $tag_query->get_tags_by_post_id($post['id']);
        $model->selected_tags = Tag::toMapString($selected_tags);
        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
            $tag_query->delete_all_post_tag($model->id);
            if ($model->update($post->id)) {
                $tag_query->manage_tags($model->id, $model->tags);
                if ($model->privacy_id == 2) {
                    //TODO select friend
                    return Yii::$app->runAction('post/select-friend', ['post_id' => $model->id]);
                } else {
                    return Yii::$app->runAction('post/index');
                }
            } else {
                return $this->render('edit', ['model' => $model, 'tags' => $tags, 'is_updated' => false]);
            }
        }
        return $this->render('edit', ['model' => $model, 'tags' => $tags]);
    }

    public function actionIndex()
    {
        return Yii::$app->runAction('post/list', ['page' => 0]);
    }

    public function actionList($page)
    {
        $post_query = PostQuery::getInstance();
        $owner_id = Yii::$app->user->getId();
        $posts = $post_query->getPostByOwnerId($owner_id, $page);

        return $this->render('index', ['posts' => $posts, 'page' => $page]);
    }

    public function actionSelectFriend($post_id)
    {
        $friend_query = FriendQuery::getInstance();
        $user_id = Yii::$app->user->getId();
        $friends = $friend_query->get_all_friend($user_id);
        var_dump($friends);die;
        //TODO select friend
    }

    private function assign_post_to_form($post, $form)
    {
        $form->id = $post->id;
        $form->title = $post->title;
        $form->content = $post->content;
        $form->comment = $post->commentable;
        $form->privacy_id = $post->privacy_id;
        $form->time = $post->time;
    }
}