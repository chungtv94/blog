<?php
/**
 * Created by PhpStorm.
 * User: tuanna
 * Date: 10/23/2015
 * Time: 1:02 PM
 */
namespace frontend\controllers;

use frontend\models\UserQuery;
use Yii;
use yii\base\ErrorException;
use yii\web\Controller;

class SearchController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionUser()
    {
        if (Yii::$app->request->post()) {
            $keyword = Yii::$app->request->post()['keyword'];
            return Yii::$app->runAction('search/user-result', ['keyword' => $keyword, 'page' => 0]);
        }
        return $this->render('user');
    }

    public function actionUserResult($keyword, $page)
    {
        if (Yii::$app->request->post()) {
            $keyword = Yii::$app->request->post()['keyword'];
            $page = 0;
        }
        $userQuery = UserQuery::getInstance();
        if ($page < 0) {
            throw new ErrorException();
        }
        $from_record = $page * 5;
        $results = $userQuery->searchUserByKeyword($keyword, $from_record);
        return $this->render('user_result', ['page' => $page, 'keyword' => $keyword, 'results' => $results]);
    }
}