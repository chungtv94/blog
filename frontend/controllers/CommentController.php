<?php
/**
 * Created by PhpStorm.
 * User: tuanna
 * Date: 10/22/2015
 * Time: 1:39 PM
 */
namespace frontend\controllers;

use frontend\models\CommentQuery;
use frontend\models\UserQuery;
use Yii;
use yii\web\Controller;

class CommentController extends Controller
{
    public function actionCreate()
    {
        if (isset($_POST['owner_id']) && isset($_POST['post_id']) && isset($_POST['content'])) {
            $post_id = $_POST['post_id'];
            $owner_id = $_POST['owner_id'];
            $comment_query = CommentQuery::getInstance();
            if (isset($_POST['content']) && !empty($_POST['content'])) {
                $content = $_POST['content'];
                if ($comment = $comment_query->create($post_id, $owner_id, $content)) {
                    $user_query = UserQuery::getInstance();
                    $user = $user_query->getUserById($owner_id);
                    $username = empty($user['full_name']) ? "No name" : $user['full_name'];
                    $avatar = empty($user['image']) ? "default.jpg" : $user['image'];

                    echo
                        '<div class="box-comment">'.
                        '<img class="img-circle img-sm" src="'.Yii::$app->request->baseUrl . '/images/' . $avatar.'" alt="Avatar">'.
                            '<div class="comment-text">'.
                                '<span class="username">'.
                                    $username.
                                    '<span class="text-muted pull-right">'.$comment['created_at'].'</span>'.
                                    '<a class="del_comment text-muted pull-right" id="' . $comment['id'] . '"><i
                                      class="fa fa-trash"></i></a>
                                    <br>'.
                                '</span>'.
                                $content.
                            '</div>'.
                        '</div>';
                } else {
                    echo "NO";
                }
            }
        } else {
            echo "NO";
        }
    }

    public function actionDelete()
    {
        if (isset($_POST['id'])) {
            $comment_query = CommentQuery::getInstance();
            if ($comment_query->delete($_POST['id'])) {
                echo "YES";
            } else {
                echo "NO";
            }
        } else {
            echo "NO";
        }
    }
}