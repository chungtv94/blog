<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/24/2015
 * Time: 10:15 AM
 */
namespace frontend\controllers;

use app\models\Friend;
use app\models\Notification;
use yii\web\Controller;

class AjaxController extends Controller
{
    public function actionAcceptFriend()
    {
        if (isset($_POST['notify_id'])) {
            $notify_id = $_POST['notify_id'];
            $notification = Notification::findOne(['id' => $notify_id]);
            $send_request_user_id = $notification['from_user_id'];
            $received_request_user_id = $notification['to_user_id'];
            $friend = Friend::findOne(['send_request_user_id' => $send_request_user_id, 'received_request_user_id' => $received_request_user_id]);
            $friend->is_accepted = true;
            if ($friend->save()) {
                Notification::deleteAll(['id'=>$notify_id]);
                echo "YES";
            } else {
                echo "NO";
            }
        } else {
            echo "NO";
        }
    }
}