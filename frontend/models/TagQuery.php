<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 5:57 AM
 */
namespace frontend\models;

use app\models\PostTag;
use app\models\Tag;
use yii\db\Query;

class TagQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new TagQuery();
        } else {
            return static::$query;
        }
    }

    public function get_tags_by_post_id($post_id)
    {
        $model = \Yii::$app->db->createCommand(
            'SELECT tag.id,name' .
            ' FROM tag LEFT JOIN post_tag ON tag_id = tag.id' .
            ' WHERE post_id=:post_id'
        );
        $model->bindValues([':post_id' => $post_id]);
        return $model->queryAll();
    }

    public function getAll()
    {
        return (new Query())->select(['id', 'name'])->from('tag')->all();
    }

    public function manage_tags($post_id, $post_tags)
    {
        for ($i = 0; $i < count($post_tags); $i++) {
            $name = $post_tags[$i];
            $tag = Tag::findOne(['name' => $name]);
            if ($tag == null) {
                $tag = new Tag();
                $tag->name = $name;
                $tag->save();
            }
            $post_tag = PostTag::findOne(['post_id' => $post_id, 'tag_id' => $tag['id']]);
            if ($post_tag == null) {
                $post_tag = new PostTag();
                $post_tag->post_id = $post_id;
                $post_tag->tag_id = $tag['id'];
                $post_tag->save();
            }
        }
    }

    public function delete_all_post_tag($post_id)
    {
        $connection = \Yii::$app->getDb();
        $model = $connection->createCommand()->delete('post_tag', ['post_id' => $post_id]);
        return $model->execute();
    }

}