<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:07 PM
 */
namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class SettingForm extends Model
{

    public $first_name;
    public $last_name;
    public $address;
    public $username;
    public $phone_number;
    public $hobby;
    public $work;
    public $desc;
    public $email;
    public $image;
    public $imageFile;

    private $user;
    private $id;

    public function __construct()
    {
        $this->id = Yii::$app->user->getId();
        $user = $this->getUser($this->id);

        $this->email = $user->email;
        $this->first_name = $user->first_name;
        $this->last_name = $user->last_name;
        $this->username = $user->username;
        $this->address = $user->address;
        $this->phone_number = $user->phone_number;
        $this->desc = $user->desc;
        $this->hobby = $user->hobby;
        $this->work = $user->work;
        $this->image = $user->image;
    }

    public function rules()
    {
        return [
            ['first_name', 'string'],
            ['last_name', 'string'],
            ['username', 'string', 'min' => 3, 'max' => 30],
            ['address', 'string'],
            ['phone_number', 'string', 'min' => 10, 'max' => 20],
            ['desc', 'string', 'min' => 6],
            ['work', 'string'],
            ['hobby', 'string'],
            ['imageFile', 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg', 'checkExtensionByMimeType' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFile' => 'Ảnh đại diện',
            'desc' => 'Mô tả về bản thân',
            'address' => 'Địa chỉ hiện tại',
            'phone_number' => 'Số điện thoại cá nhân',
            'hobby'=>'Sở thích',
            'work'=>'Công việc hiện tại',
        ];
    }

    public function updateInfo()
    {
        $model = $this->getUser($this->id);

        if ($this->upload()) {
            $model->image = $this->image;
        }
        if ($this->validate() && !$this->hasErrors()) {
            $model->first_name = $this->first_name;
            $model->last_name = $this->last_name;
            $model->username = $this->username;
            $model->address = $this->address;
            $model->desc = $this->desc;
            $model->phone_number = $this->phone_number;
            $model->work = $this->work;
            $model->hobby = $this->hobby;

            return $model->save();
        } else {
            return false;
        }
    }

    private function getUser($id)
    {
        if ($this->user === null) {
            $this->user = User::findIdentity($id);
        }
        return $this->user;
    }

    public function upload()
    {
        if (!$this->hasErrors()) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            if ($this->imageFile == null) {
                return false;
            }
            $this->imageFile->saveAs('images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->image = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }
}