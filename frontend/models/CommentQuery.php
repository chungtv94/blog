<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/22/2015
 * Time: 1:43 PM
 */
namespace frontend\models;

use app\models\Comment;
use Yii;

class CommentQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new CommentQuery();
        } else {
            return static::$query;
        }
    }

    public function create($post_id, $owner_id, $content)
    {
        $comment = new Comment();
        $comment->post_id = $post_id;
        $comment->owner_id = $owner_id;
        $comment->content = $content;
        $comment->created_at = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $comment->save();
        return $comment;
    }

    public function get_comments($post_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT c.id as id,content,c.created_at,image as avatar,CONCAT(first_name," ",last_name) as full_name,owner_id' .
            ' FROM user LEFT JOIN comment as c ON owner_id = user.id' .
            ' WHERE post_id=:post_id'
        );
        $model->bindValues([':post_id' => $post_id]);
        return $model->queryAll();
    }

    public function delete($id)
    {
        $model = Yii::$app->db->createCommand(
            'DELETE ' .
            ' FROM comment ' .
            ' WHERE id=:id'
        );
        $model->bindValues([':id' => $id]);
        return $model->execute();
    }
}