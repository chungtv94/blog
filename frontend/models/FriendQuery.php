<?php

/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 11/9/2015
 * Time: 9:52 AM
 */
namespace frontend\models;
use Yii;

class FriendQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new FriendQuery();
        } else {
            return static::$query;
        }
    }

    public function get_all_friend($user_id)
    {
        $query = Yii::$app->db->createCommand(
            ' SELECT received_request_user_id,CONCAT(first_name," ",last_name) as full_name,image,group.name ' .
            ' FROM user LEFT JOIN friend ON user.id = received_request_user_id LEFT JOIN "group" ON group_id = group.id ' .
            ' WHERE send_request_user_id=:user_id AND is_accepted = 1'
        );
        $query->bindValues([':user_id' => $user_id]);
        return $query->queryAll();
    }

}