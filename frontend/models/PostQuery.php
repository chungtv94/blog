<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:07 PM
 */
namespace frontend\models;

use app\models\Friend;
use app\models\FriendGroup;
use Yii;

class PostQuery
{

    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new PostQuery();
        } else {
            return static::$query;
        }
    }

    public function getPostById($post_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id,title,content,p.image as image,privacy_id,time,p.created_at,user.image as avatar,CONCAT(first_name," ",last_name) as full_name,owner_id ' .
            ' FROM post as p LEFT JOIN user ON owner_id = user.id' .
            ' WHERE p.id = :id'
        );
        $model->bindValues([':id' => $post_id]);
        return $model->queryOne();
    }

    public function getBeforePostForId($owner_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id,title,p.image as image,time,content,privacy_id,user.image as avatar,CONCAT(first_name," ",last_name) as full_name,owner_id ' .
            ' FROM post as p LEFT JOIN user ON owner_id = user.id ' .
            ' WHERE owner_id = :owner_id LIMIT 5'
        );
        $model->bindValues(['owner_id' => $owner_id]);
        return $model->queryAll();
    }

    public function getPostByOwnerId($owner_id, $page)
    {
        $from_record = $page * 10;
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id,title,content,p.image as image,time,privacy_id,p.created_at ' .
            ' FROM post as p ' .
            ' WHERE owner_id =:owner_id LIMIT :from_record,10'
        );
        $model->bindValues([':owner_id' => $owner_id, ':from_record' => $from_record]);
        return $model->queryAll();
    }

    public function readable($user_id, $owner_id, $post_id, $privacy)
    {
        if ($user_id == $owner_id)
            return true;
        switch ($privacy) {
            case 1:
                return false;
            case 2:
                $model = FriendGroup::findOne(['user_id' => $user_id, 'post_id' => $post_id]);
                if ($model == null) {
                    return false;
                } else {
                    return true;
                }
            case 3:
                $model = Friend::findOne(['send_request_user_id' => $user_id, 'received_request_user_id' => $owner_id, 'is_accepted' => true]);
                if ($model == null) {
                    return false;
                } else {
                    return true;
                }
            case 4:
                return true;
            default:
                return true;
        }
    }

}