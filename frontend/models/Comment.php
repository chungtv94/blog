<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/22/2015
 * Time: 1:41 PM
 */
namespace app\models;

use yii\db\ActiveRecord;

class Comment extends ActiveRecord
{
    public static function tableName()
    {
        return 'comment';
    }
}