<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 11:50 AM
 */
namespace app\models;

use yii\db\ActiveRecord;

class FriendGroup extends ActiveRecord
{
    public static function tableName()
    {
        return 'friend_group';
    }
}