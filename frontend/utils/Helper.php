<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/21/2015
 * Time: 11:33 AM
 */
namespace frontend\utils;
class Helper
{
    static $SEND_REQUEST_MAKE_FRIEND = 1;
    static $RECEIVED_REQUEST_MAKE_FRIEND = 2;
    public static function print_privacy($privacy_id)
    {
        switch ($privacy_id) {
            case 1:
                return 'Private';
            case 2:
                return 'Protected 1';
            case 3:
                return 'Protected 2';
            case 4:
                return 'Public';
            default:
                return null;
        }
    }

    public static function permission_request()
    {
        if (\Yii::$app->user->isGuest)
            return false;
        return true;
    }

}