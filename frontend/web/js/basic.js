$(document).ready(function () {
    $('.del_comment').click(function () {
        var id = $(this).attr('id');
        var data = "id=" + id;
        var container = $(this).parent().parent().parent();
        $.ajax({
            type: "POST",
            url: "?r=comment/delete",
            data: data,
            success: function (respone) {
                if (respone == "NO") {
                    alert("Comment chưa được xóa, vui lòng thử lại!");
                } else {
                    container.slideUp('slow', function () {
                        container.remove();
                    });
                }
            }
        });
    });
    $('.add_friend').click(function () {
        $('#choice_group').show(1500);
        $('.add_friend').hide(1000);
    });
    $('.accept_friend').click(function () {
        var id = $(this).attr('id');
        var data = "notify_id=" + id;
        var container = $(this).parent().parent();
        var parent = container.parent();
        $.ajax({
            type: "POST",
            url: "?r=ajax/accept-friend",
            data: data,
            success: function (response) {
                if (response == "YES") {
                    alert("Bạn đã có thêm 1 người mới trong danh sách bạn bè.");
                    parent.slideUp('slow', function () {
                        parent.remove();
                    });
                } else {
                    alert("Có lỗi xảy ra, vui lòng thử lại lần nữa.");
                }
            }
        });
    });
    $('.content_cmt').keydown(function (e) {
        if (e.keyCode == 13) {
            var content = $(this).val();
            var data = $(this).attr("id");
            var params = data + "&content=" + content;
            var container_cmt = $('#box-comment');
            $.ajax({
                type: "POST",
                url: "?r=comment/create",
                data: params,
                success: function (response) {
                    if (response != "NO") {
                        container_cmt.append(response);
                        $(this).val = "";
                    }
                }
            });
        }
    });
});